package com.example.aplikasilogindanregister.api;

import com.example.aplikasilogindanregister.model.login.Login;
import com.example.aplikasilogindanregister.model.register.Register;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login.php")
    Call<Login> loginResponse(
      @Field("username") String userName,
      @Field("password") String Password
    );

    @FormUrlEncoded
    @POST("register.php")
    Call<Register> registerResponse(
            @Field("username") String userName,
            @Field("password") String Password,
            @Field("name") String name
    );

}
